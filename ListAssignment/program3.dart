/*
3.
Take the List of length 6 from the user and print true if all the elements are
divisible by both 3 and 5 (length of list is hardcoded i.e 6)

Input: [15,30,45,10,9]
Output: false
Input: [15,30,45,60,90]
Output: true
*/


import 'dart:io';
void main(){
  List sd=new List.empty(growable: true);
  stdout.write("How many elements add in the list: ");
  int x=int.parse(stdin.readLineSync()!);
  int count = 0;
  for(int i=1;i<=x;i++){
    stdout.write("Enter ${i}st element: ");
    int y=int.parse(stdin.readLineSync()!);
    sd.add(y);
  }
  print(sd);

  for (int i = 0; i<sd.length; i++) {
    if (sd.elementAt(i)%3 == 0 && sd.elementAt(i)%5 == 0) {
      count++;
    }
  }

  if (count == sd.length) {
    print("True");
  } else {
    print("False");
  }

}