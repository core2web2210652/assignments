//largest element in list
import 'dart:io';

void main(){
  List<int> list1=List.empty(growable: true);
  stdout.write("enter length of list");
  int val=int.parse(stdin.readLineSync()!);
  for(int i=0;i<val;i++){
    stdout.write("enter element at list");
    int ele=int.parse(stdin.readLineSync()!);
    list1.add(ele);
  }
  list1.sort();
  var a=list1.last;
  
  print("largest element is:$a");
}