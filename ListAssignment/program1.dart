/*

1.
Take a list of length 6 from the user and find how many numbers in
the list are even numbers; (length of list is hardcoded i.e 6)
Input: [10,3,5,6,4]
Output: 3

*/
import 'dart:io';
void main(){
  List list=List.empty(growable: true);
  stdout.write("enter number of elements in list:- ");
  int num=int.parse(stdin.readLineSync()!);
  int count=0;
  for(int i=1;i<=num;i++){

      stdout.write("enter ${i}st element: ");
      int num1 =int.parse(stdin.readLineSync()!);
      list.add(num1);
  }
  print(list);

  for(int i=0;i<list.length;i++){
    if(list[i]%2==0){
      count++;
    }
  }
  print("number of even count in list:-$count");
}

