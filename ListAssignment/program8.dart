//WAP to print largest element in list
import 'dart:io';

void main(){
  List<int> list1=List.empty(growable: true); 
  print("enter length of list");
  int len=int.parse(stdin.readLineSync()!);
  for(int i=0;i<len;i++){
    print("enter element in list:-");
  int val=int.parse(stdin.readLineSync()!);
    list1.add(val);
  }
  int maxval=0;
  list1.forEach((element) {
    if(element>maxval){
      maxval=element;
    }
  });
  print("maximum element in list:-$maxval");
}