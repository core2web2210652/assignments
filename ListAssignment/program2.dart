/*2.
Take the list of length 5 from the user and calculate the sum of the numbers
present in the list (length of list is hardcoded i.e 5)

Input: [1,2,3,4,5]
Output: Sum of numbers is : 15*/

import "dart:io";
void main(){
  List obj=new List.empty(growable: true);
  stdout.write("how many elements add in List: ");
  int x=int.parse(stdin.readLineSync()!);
  int sum=0;
  for(int i=1;i<=x;i++){
    stdout.write("Enter ${i}st element: ");
  int y=int.parse(stdin.readLineSync()!);
  obj.add(y);

  }
  print(obj);

  for(int i=0;i<obj.length;i++){
    int temp = obj.elementAt(i);
    sum += temp;
  }
  print("Sum of elements in list: $sum");
}
