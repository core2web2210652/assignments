import 'dart:io';
void main(){
  stdout.write("enter no of rows");
  int val=int.parse(stdin.readLineSync()!);
  int a=1;
  for(int i=0;i<val;i++){
    for(int j=1;j<val-i;j++){
      stdout.write("  ");
    }
    
    for(int k=i+1;k>=1;k--){
      int product=a*k;
      stdout.write(" $product");  
    }
    a++;
    print("");
  }
}